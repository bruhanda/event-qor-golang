package event

import (
	"fmt"
	"html/template"
	"net/http"

	"github.com/qor/admin"
	"github.com/qor/qor/utils"
	"github.com/jinzhu/gorm"
	"github.com/qor/publish2"
)

func init() {
	admin.RegisterViewPath("github.com/qor/event/views")
}

// ActionBar stores configuration about a action bar.
type Eventer interface {

}